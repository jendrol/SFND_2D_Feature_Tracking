# SFND 2D Feature Tracking

<img src="images/keypoints.png" width="820" height="248" />

The idea of the camera course is to build a collision detection system - that's the overall goal for the Final Project. As a preparation for this, you will now build the feature tracking part and test various detector / descriptor combinations to see which ones perform best. This mid-term project consists of four parts:

* First, you will focus on loading images, setting up data structures and putting everything into a ring buffer to optimize memory load. 
* Then, you will integrate several keypoint detectors such as HARRIS, FAST, BRISK and SIFT and compare them with regard to number of keypoints and speed. 
* In the next part, you will then focus on descriptor extraction and matching using brute force and also the FLANN approach we discussed in the previous lesson. 
* In the last part, once the code framework is complete, you will test the various algorithms in different combinations and compare them with regard to some performance measures. 

See the classroom instruction and code comments for more details on each of these parts. Once you are finished with this project, the keypoint matching part will be set up and you can proceed to the next lesson, where the focus is on integrating Lidar points and on object detection using deep-learning. 

## Dependencies for Running Locally
* cmake >= 2.8
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* OpenCV >= 4.1
  * This must be compiled from source using the `-D OPENCV_ENABLE_NONFREE=ON` cmake flag for testing the SIFT and SURF detectors.
  * The OpenCV 4.1.0 source code can be found [here](https://github.com/opencv/opencv/tree/4.1.0)
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the top level directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./2D_feature_tracking`.

## MP.1 Data Buffer Optimization

Deque container is used instead of vector. Deque offers constant complexity for pushing item to back and poping item from front.

## MP.2 Keypoint Detection

HARRIS detection is implemented in the detKeypointsHarris function. FAST, BRISK, ORB, AKAZE, and SIFT are implemented using the detKeypointsModern, detector is selectable using the detectorType argument. All detectors are created using default argument values.

## MP.3 Keypoint Removal

Keypoints that are outside of the rectangle are removed from the list using Rect::contains function. Rectangle roughly represents area covered by preceding car. 

## MP.4 Keypoint Descriptors

Descriptors BRIEF, ORB, FREAK, AKAZE and SIFT are implemented. Descriptor is selectable by setting the descriptorType.

## MP.5 Descriptor Matching

FLANN matching and KNN selection is implemented. 
Binary descriptors are converted to floats before matching by FLANN.

Sources:
https://stackoverflow.com/questions/43830849/opencv-use-flann-with-orb-descriptors-to-match-features

https://answers.opencv.org/question/59996/flann-error-in-opencv-3/

https://stackoverflow.com/questions/29694490/flann-error-in-opencv-3

## MP.6 Descriptor Distance Ratio

Distance ration 0.8 is used for KNN selector.

## MP.7 Performance Evaluation 1

See measurements/MP_7.csv.

Highest count of keypoints is produced by the BRISK detector.

### Keypoint size

Following table describes distribution of keypoint sizes.

| Detector | Min | Max | Mean | Median |
| -------- | --- | --- | ---- | ------ |
| Shi-Tomasi | 4 | 4 | 4 | 4 |
| Harris | 6 | 6 | 6 | 6 |
| FAST | 7 | 7 | 7 | 7 |
| BRISK | 8.4 | 72 | 15.52 | 21.94 |
| ORB | 31 | 111.08 | 44.64 | 55.99 |
| AKAZE | 4.8 | 27.15 | 5.71 | 7.7 |
| SIFT | 1.8 | 51.7 | 3.2 | 5.04 |

## MP.8 Performance Evaluation 2

See measurements/MP_8_9.csv.

Highest count of matches is achieved using BRISK detector with BRIEF descriptor.

## MP.9 Performance Evaluation 3

See measurements/MP_8_9.csv and measurements/MP_8_9_averages.csv.

To select TOP 3 detector / descriptor combinations, I've decide to use following criteria:
1. Number of matches
2. Time of detection + time of description
3. Patent-free

BRISK detector provided highest count of matches. Fastest total time was achieved when paired with BRIEF descriptor. Second best time was achieved with ORB descriptor.

When processing time is critical, FAST + BRIEF provides over 100 keypoints in couple of milliseconds.